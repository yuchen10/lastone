import bagel.*;
import bagel.util.Point;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
/**
 * An example Bagel game.
 *
 * @author Eleanor McMurtry
 */
public class BagelTest extends AbstractGame {
    private Image tree[] = new Image[4];
    private Image gatherer[] = new Image[4];
    private Image background;
    int counterTree = 0;
    int counterGatherer = 0;
    int towards[] = {1, 2, 3, 4};
    int counter = 0;
    int direction[] = {0, 0};
    Point pTree[] = new Point[5];
    Point pGatherer[] = new Point[5];
    BufferedReader reader;

    public BagelTest() throws IOException {
        super(1024, 768, "fuck");
//        tree[0] = new Image("res/images/tree.png");
//        gatherer[0] = new Image("res/images/gatherer.png");
        background = new Image("res/images/background.png");
        reader = new BufferedReader(new FileReader("res/worlds/test.csv"));
        String line;

        while ((line = reader.readLine()) != null) {
            String item[] = line.split(",");//CSV格式文件为逗号分隔符文件，这里根据逗号切分
            int itemX = Integer.parseInt(item[1]);
            int itemY = Integer.parseInt(item[2]);
            if (item[0].equals("Tree")) {
                tree[counterTree] = new Image("res/images/tree.png");
                pTree[counterTree] = new Point(itemX / 16, itemY / 12);
                //有问题，如果初始位置不在64的倍数会强制转成64倍数
                counterTree++;
            } else {
                gatherer[counterGatherer] = new Image("res/images/gatherer.png");
                pGatherer[counterGatherer] = new Point(itemX / 16, itemY / 12);
                counterGatherer++;
            }
        }

        //p[0] = new Point(512 / 64, 384 / 64);
    }

    /**
     * The entry point for the program.
     */
//    public static void main(String[] args) throws IOException {
//        BagelTest game = new BagelTest();
//        game.run();
//    }

    @Override
    public void update(Input input) {
        double speed = 1;
        Tool tool = new Tool();
        if (input.wasPressed(Keys.ESCAPE)) {
            Window.close();
        }
        if (counter % 5 == 0) {

            direction[0] = tool.chooseDirection();
            direction[1] = tool.chooseDirection();
        }


        try {
            Thread.sleep(500);
            counter++;

        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        for (int i = 0; i < counterGatherer; i++) {
            tool.move(pGatherer, i, direction[i], speed);
            System.out.println(pGatherer[i].x);
        }

        background.drawFromTopLeft(0, 0);
        for (int i = 0; i < counterGatherer; i++) {
            gatherer[i].drawFromTopLeft(pGatherer[i].x * 16, pGatherer[i].y * 12);
            //System.out.println(pGatherer[i].y);
            //System.out.println(direction);
        }
        for (int i = 0; i < counterTree; i++) {
            tree[i].drawFromTopLeft(pTree[i].x * 16, pTree[i].y * 12);
            //System.out.println("t"+ i +"...."+pTree[i].y*64);
        }
    }



//    public void isOut(Point point){
//        if (point.x < 0 && point.x  > 11) {
//
//        }
//    }
//    public void isHome(){
//        if ((55> Math.abs(854-x)) && (55>  Math.abs(268-y))) {
//            if (t == 0) {
//                System.out.println("you are home!");
//            }
//            t++;
//
//            font.drawString("you are home!", 32, 32);
//            //Window.close();
//        }
//    }
}
