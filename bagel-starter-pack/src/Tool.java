import bagel.util.Point;

public class Tool {
    public int chooseDirection() {
        int direction = 1 + (int) (Math.random() * (4));
        return direction;
    }

    public void move(Point point[], int order, int direction, double speed) {
        if (direction == 1) {
            point[order] = new Point(point[order].x - speed, point[order].y);
        }
        if (direction == 2) {
            point[order] = new Point(point[order].x + speed, point[order].y);
            //point = new Point(point.x + speed, point.y);
        }
        if (direction == 3) {
            point[order] = new Point(point[order].x, point[order].y - speed);
            // point = new Point(point.x, point.y - speed);
        }
        if (direction == 4) {
            point[order] = new Point(point[order].x, point[order].y + speed);
            //point = new Point(point.x, point.y + speed);
        }
    }
}
